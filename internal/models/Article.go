package models

import (
	"time"
)

type Article struct {
	// gorm.Model  `json:"-"`
	Base
	Title       string     `gorm:"type:varchar(50);not null;unique" json:"title"`
	Slug        string     `gorm:"type:varchar(50);not null;unique" json:"slug"`
	Body        string     `gorm:"type:text;not null" json:"body"`
	Published   bool       `gorm:"type:boolean;not null" json:"published"`
	PublishedAt *time.Time `gorm:"type:date;default:NULL" json:"publised_at"`
	EditedAt    *time.Time `gorm:"default:NULL" json:"edited_at"`
	View        int        `gorm:"type:integer; default:0" json:"view"`
	Image       *string    `gorm:"type:varchar(255);default:NULL" json:"image"`
	CategoryID  uint       `gorm:"not null" json:"category_id"`
	Category    *Category  `gorm:"omitempty" json:"category"`
	Tags        []Tag      `gorm:"many2many:article_tags" json:"tags"`
}
