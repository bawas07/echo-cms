package models

type Tag struct {
	// gorm.Model  `json:"-"`
	Base
	Name        string    `gorm:"type:varchar(50);not null;unique" json:"name"`
	Description string    `gorm:"default:NULL" json:"description"`
	Articles    []Article `gorm:"many2many:article_tags" json:"articles"`
}
