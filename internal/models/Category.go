package models

type Category struct {
	// gorm.Model  `json:"-"`
	Base
	Title       string    `gorm:"type:varchar(50);not null;unique" json:"title"`
	Description string    `gorm:"default:NULL" json:"description"`
	Image       *string   `gorm:"type:varchar(255);default:NULL" json:"image"`
	Articles    []Article `json:"articles"`
}
