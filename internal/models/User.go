package models

import (
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	// gorm.Model  `json:"-"`
	Base
	FirstName   string     `gorm:"type:varchar(50);not null" json:"first_name"`
	LastName    string     `gorm:"type:varchar(50);not null" json:"last_name"`
	Email       string     `gorm:"type:varchar(50);not null; unique" json:"email"`
	Password    string     `gorm:"type:varchar(255);not null" json:"password"`
	Role        string     `gorm:"type:varchar(20);not null" json:"role"`
	Description string     `gorm:"default:NULL" json:"description"`
	BirthDate   time.Time  `gorm:"type:date; default:NULL" json:"birth_date"`
	Otp         *string    `gorm:"type:varchar(30);default:NULL" json:"otp"`
	OtpExpired  *time.Time `gorm:"default:NULL" json:"otp_expired"`
	Token       *string    `gorm:"type:varchar(255);default:NULL" json:"token"`
	Image       *string    `gorm:"type:varchar(255);default:NULL" json:"image"`
}

func (u *User) HashPassword() error {
	password := u.Password
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		return err
	}
	u.Password = string(bytes)
	return nil
}

func (u User) CheckPassword(input string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(input), []byte(u.Password))
	// fmt.Println(err.Error())
	return err == nil
}

func (u User) GenerateToken() (string, error) {
	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = u.ID
	claims["email"] = u.Email
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	return t, err

}
