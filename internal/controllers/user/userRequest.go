package user

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

type create struct {
	FirstName   string    `json:"first_name" validate:"required"`
	LastName    string    `json:"last_name" validate:"required"`
	Email       string    `json:"email" validate:"required,email"`
	Password    string    `json:"password" validate:"required"`
	Role        string    `json:"role" validate:"required"`
	Description string    `json:"description"`
	BirthDate   time.Time `json:"birth_date" validate:"required"`
	Image       *string   `json:"image"`
}

func (data *create) checkValidation(c echo.Context, user *models.User) error {
	err := c.Validate(data)
	if err != nil {
		return err
	}

	user.FirstName = data.FirstName
	user.LastName = data.LastName
	user.Email = data.Email
	user.Password = data.Password
	user.Role = data.Role
	user.Description = data.Description
	user.BirthDate = data.BirthDate
	user.Image = data.Image

	return nil
}
