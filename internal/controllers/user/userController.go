package user

import (
	"fmt"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/helpers"
	"gitlab.com/bawas07/echo-cms/internal/models"
	"gitlab.com/bawas07/echo-cms/internal/services"
)

// GetUser return array of all users
func GetUser(c echo.Context) error {
	var users []models.User
	err := services.GetAllUser(&users)
	if err != nil {
		return helpers.ResponseError(c, 500, "error found", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", users)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}

// GetUserByID return one user based on id
func GetUserByID(c echo.Context) error {
	var user models.User
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetUserByID(&user, id)
	fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", user)
}

func CreateUser(c echo.Context) error {
	// fmt.Println(c.["birth_date"])
	var req create
	// err := c.Bind(&user)
	err := c.Bind(&req)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	var user models.User

	err = req.checkValidation(c, &user)

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	err = user.HashPassword()
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	err = services.StoreUser(&user)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "User Created", user)
}

func UpdateUser(c echo.Context) error {
	var user models.User
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetUserByID(&user, id)
	var userNew models.User
	err = c.Bind(&userNew)

	user.FirstName = userNew.FirstName
	user.LastName = userNew.LastName
	user.Email = userNew.Email
	user.Password = userNew.Password
	user.Role = userNew.Role
	user.Description = userNew.Description
	user.BirthDate = userNew.BirthDate
	user.Otp = userNew.Otp

	err = services.UpdateUser(&user)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Data Updated", user)
}

// DeleteUser delete one user based on id
func DeleteUser(c echo.Context) error {
	var user models.User
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetUserByID(&user, id)
	fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	err = services.DeleteUser(&user)
	if err != nil {
		return helpers.ResponseSuccess(c, 500, "", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Deleted", user)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}
