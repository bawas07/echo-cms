package tag

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

type create struct {
	Name        string `json:"name" validate:"required"`
	Description string `json:"description"`
}

func (data *create) checkValidation(c echo.Context, cat *models.Tag) error {
	err := c.Validate(data)
	if err != nil {
		return err
	}

	cat.Name = data.Name
	cat.Description = data.Description

	return nil
}
