package tag

import (
	"fmt"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/helpers"
	"gitlab.com/bawas07/echo-cms/internal/models"
	"gitlab.com/bawas07/echo-cms/internal/services"
)

// GetTag return array of all tags
func GetTag(c echo.Context) error {
	var tag []models.Tag
	err := services.GetAllTag(&tag)
	if err != nil {
		return helpers.ResponseError(c, 500, "error found", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", tag)
}

// GetTagByID return one tag based on id
func GetTagByID(c echo.Context) error {
	var tag models.Tag
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetTagByID(&tag, id)
	fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", tag)
}

func CreateTag(c echo.Context) error {
	// fmt.Println(c.["birth_date"])
	var req create
	// err := c.Bind(&user)
	err := c.Bind(&req)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	var tag models.Tag

	err = req.checkValidation(c, &tag)

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	err = services.StoreTag(&tag)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Tag Created", tag)
}

func UpdateTag(c echo.Context) error {
	var tag models.Tag
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetTagByID(&tag, id)
	var tagNew models.Tag
	err = c.Bind(&tagNew)

	tag.Name = tagNew.Name
	tag.Description = tagNew.Description

	err = services.UpdateTag(&tag)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Data Updated", tag)
}

// DeleteUser delete one user based on id
func DeleteTag(c echo.Context) error {
	var tag models.Tag
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetTagByID(&tag, id)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	err = services.DeleteTag(&tag)
	if err != nil {
		return helpers.ResponseSuccess(c, 500, "", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Deleted", tag)
}
