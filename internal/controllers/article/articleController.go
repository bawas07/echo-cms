package article

import (
	"fmt"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/helpers"
	"gitlab.com/bawas07/echo-cms/internal/models"
	"gitlab.com/bawas07/echo-cms/internal/services"
)

// GetArticle return array of all article
func GetArticle(c echo.Context) error {
	var article []models.Article
	err := services.GetAllArticle(&article)
	if err != nil {
		return helpers.ResponseError(c, 500, "error found", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", article)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}

// GetArticleByID return one article based on id
func GetArticleByID(c echo.Context) error {
	var article models.Article
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetArticleByID(&article, id)
	// fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", article)
}

func CreateArticle(c echo.Context) error {
	// fmt.Println(c.["birth_date"])
	var req create
	// err := c.Bind(&user)
	err := c.Bind(&req)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	var article models.Article

	err = req.checkValidation(c, &article)

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	err = services.StoreArticle(&article, req.TagID)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "User Created", article)
}

func UpdateArticle(c echo.Context) error {
	var article models.Article
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetArticleByID(&article, id)
	var artNew create
	err = c.Bind(&artNew)

	article.Title = artNew.Title
	article.Slug = artNew.Slug
	article.Body = artNew.Body
	article.Published = artNew.Published
	article.PublishedAt = artNew.PublishedAt
	article.EditedAt = artNew.EditedAt
	article.View = artNew.View
	article.Image = artNew.Image
	article.CategoryID = artNew.CategoryID

	err = services.UpdateArticle(&article, artNew.TagID)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Data Updated", article)
}

// DeleteArticle delete one user based on id
func DeleteArticle(c echo.Context) error {
	var article models.Article
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetArticleByID(&article, id)
	fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	err = services.DeleteArticle(&article)
	if err != nil {
		return helpers.ResponseSuccess(c, 500, "", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Deleted", article)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}
