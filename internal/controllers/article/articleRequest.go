package article

import (
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

type create struct {
	Title       string     `json:"title" validate:"required"`
	Slug        string     `json:"slug" validate:"required"`
	Body        string     `json:"body" validate:"required"`
	Published   bool       `json:"published"`
	PublishedAt *time.Time `json:"published_at"`
	EditedAt    *time.Time `json:"edited_at"`
	View        int        `json:"view"`
	Image       *string    `json:"image"`
	CategoryID  uint       `json:"category_id" validate:"required"`
	TagID       []int      `json:"tag_id"`
}

func (data *create) checkValidation(c echo.Context, art *models.Article) error {
	err := c.Validate(data)
	if err != nil {
		return err
	}

	art.Title = data.Title
	art.Slug = data.Slug
	art.Body = data.Body
	art.Published = data.Published
	art.PublishedAt = data.PublishedAt
	art.EditedAt = data.EditedAt
	art.View = data.View
	art.Image = data.Image
	art.CategoryID = data.CategoryID

	return nil
}
