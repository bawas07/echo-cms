package category

import (
	"fmt"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/helpers"
	"gitlab.com/bawas07/echo-cms/internal/models"
	"gitlab.com/bawas07/echo-cms/internal/services"
)

// GetCategory return array of all categories
func GetCategory(c echo.Context) error {
	var categories []models.Category
	err := services.GetAllCategory(&categories)
	if err != nil {
		return helpers.ResponseError(c, 500, "error found", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", categories)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}

// GetCategoryByID return one category based on id
func GetCategoryByID(c echo.Context) error {
	var cat models.Category
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetCategoryByID(&cat, id)
	fmt.Println(err)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 200, "Data Retrieved", cat)
}

func CreateCategory(c echo.Context) error {
	// fmt.Println(c.["birth_date"])
	var req create
	// err := c.Bind(&user)
	err := c.Bind(&req)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	var category models.Category

	err = req.checkValidation(c, &category)

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}

	err = services.StoreCategory(&category)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Category Created", category)
}

func UpdateCategory(c echo.Context) error {
	var category models.Category
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetCategoryByID(&category, id)
	var catNew models.Category
	err = c.Bind(&catNew)

	category.Title = catNew.Title
	category.Description = catNew.Description
	category.Image = catNew.Image

	err = services.UpdateCategory(&category)
	if err != nil {
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	return helpers.ResponseSuccess(c, 201, "Data Updated", category)
}

// DeleteUser delete one user based on id
func DeleteCategory(c echo.Context) error {
	var cat models.Category
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return helpers.ResponseError(c, 400, "Please insert the correct id", err)
	}
	err = services.GetCategoryByID(&cat, id)
	if err != nil {
		if err.Error() == "record not found" {
			return helpers.ResponseSuccess(c, 200, "record not found", nil)
		}
		return helpers.ResponseError(c, 500, err.Error(), nil)
	}
	err = services.DeleteCategory(&cat)
	if err != nil {
		return helpers.ResponseSuccess(c, 500, "", err)
	}
	return helpers.ResponseSuccess(c, 200, "Data Deleted", cat)
	// return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}
