package category

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

type create struct {
	Title       string  `json:"title" validate:"required"`
	Description string  `json:"description"`
	Image       *string `json:"image"`
}

func (data *create) checkValidation(c echo.Context, cat *models.Category) error {
	err := c.Validate(data)
	if err != nil {
		return err
	}

	cat.Title = data.Title
	cat.Description = data.Description
	cat.Image = data.Image

	return nil
}
