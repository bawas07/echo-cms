package controllers

import (
	"net/http"

	"gitlab.com/bawas07/echo-cms/internal/helpers"

	"github.com/labstack/echo/v4"
)

// HelloWorld is a controller to check the service is running
func HelloWorld(c echo.Context) error {
	return c.JSON(http.StatusOK, helpers.TemplateResponseSuccess(200, "Hello World", nil))
}
