package auth

import (
	"github.com/labstack/echo/v4"
)

type loginRequest struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

func (data *loginRequest) checkValidation(c echo.Context) error {
	err := c.Validate(data)
	if err != nil {
		return err
	}

	return nil
}
