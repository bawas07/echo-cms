package auth

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/helpers"
	"gitlab.com/bawas07/echo-cms/internal/models"
	"gitlab.com/bawas07/echo-cms/internal/services"
)

func Login(c echo.Context) error {
	var body loginRequest
	err := c.Bind(&body)

	if err != nil {
		return helpers.ResponseError(c, 400, "error found", err)
	}

	err = body.checkValidation(c)

	if err != nil {
		return helpers.ResponseError(c, 400, "error found", err)
	}

	var user models.User
	err = services.FindByEmailUser(&user, body.Email)
	if err != nil {
		return helpers.ResponseError(c, 401, "invalid login credentials", err)
	}

	status := user.CheckPassword(body.Password)
	if status == true {
		return helpers.ResponseError(c, 401, "invalid login credentials, password", nil)
	}

	t, err := user.GenerateToken()
	if err != nil {
		return helpers.ResponseError(c, 500, "", err)
	}

	var tokens = map[string]string{"token": t}

	return helpers.ResponseSuccess(c, 200, "", tokens)

}
