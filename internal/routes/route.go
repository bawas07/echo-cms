package routes

import (
	"gitlab.com/bawas07/echo-cms/internal/controllers"
	"gitlab.com/bawas07/echo-cms/internal/controllers/article"
	"gitlab.com/bawas07/echo-cms/internal/controllers/auth"
	"gitlab.com/bawas07/echo-cms/internal/controllers/category"
	"gitlab.com/bawas07/echo-cms/internal/controllers/tag"
	"gitlab.com/bawas07/echo-cms/internal/controllers/user"

	"github.com/labstack/echo/v4"
)

// RouteAPI is a collection of api routes
func RouteAPI(api *echo.Group) {
	v1 := api.Group("/v1")

	v1.GET("", controllers.HelloWorld)

	// v1.POST("/login", auth.Login)

	// userRoute := v1.Group("/users")
	// userRoute.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	// userRoute.GET("", user.GetUser)
	// userRoute.GET("/:id", user.GetUserByID)
	// userRoute.POST("", user.CreateUser)
	// userRoute.PUT("/:id", user.UpdateUser)
	// userRoute.DELETE("/:id", user.DeleteUser)

}

// RouteCMSAPI is a collection of cms's api's routes
func RouteCMSAPI(api *echo.Group) {
	v1 := api.Group("/v1")

	v1.GET("", controllers.HelloWorld)

	v1.POST("/login", auth.Login)

	userRoute := v1.Group("/users")
	// userRoute.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	userRoute.GET("", user.GetUser)
	userRoute.GET("/:id", user.GetUserByID)
	userRoute.POST("", user.CreateUser)
	userRoute.PUT("/:id", user.UpdateUser)
	userRoute.DELETE("/:id", user.DeleteUser)

	categoryRoute := v1.Group("/categories")
	// categoryRoute.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	categoryRoute.GET("", category.GetCategory)
	categoryRoute.GET("/:id", category.GetCategoryByID)
	categoryRoute.POST("", category.CreateCategory)
	categoryRoute.PUT("/:id", category.UpdateCategory)
	categoryRoute.DELETE("/:id", category.DeleteCategory)

	tagRoute := v1.Group("/tags")
	// categoryRoute.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	tagRoute.GET("", tag.GetTag)
	tagRoute.GET("/:id", tag.GetTagByID)
	tagRoute.POST("", tag.CreateTag)
	tagRoute.PUT("/:id", tag.UpdateTag)
	tagRoute.DELETE("/:id", tag.DeleteTag)

	articleRoute := v1.Group("/articles")
	// categoryRoute.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
	articleRoute.GET("", article.GetArticle)
	articleRoute.GET("/:id", article.GetArticleByID)
	articleRoute.POST("", article.CreateArticle)
	articleRoute.PUT("/:id", article.UpdateArticle)
	articleRoute.DELETE("/:id", article.DeleteArticle)

}
