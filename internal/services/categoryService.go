package services

import (
	"gitlab.com/bawas07/echo-cms/config"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

// GetAllCategory returl all record in table Category
func GetAllCategory(model *[]models.Category) error {

	db := config.DBConn()
	defer db.Close()
	err := db.Find(model).Error
	if err != nil {
		return err
	}
	return nil
}

// GetCategoryByID returl record in table category based on id
func GetCategoryByID(model *models.Category, id int) error {

	db := config.DBConn()
	defer db.Close()
	err := db.First(model, id).Error
	if err != nil {
		return err
	}
	return nil
}

// StoreCategory used to store a record into table category
func StoreCategory(model *models.Category) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Create(model).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateCategory(model *models.Category) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Save(model).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteCategory(model *models.Category) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Delete(model).Error
	if err != nil {
		return err
	}
	return nil
}
