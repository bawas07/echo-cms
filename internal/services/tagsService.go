package services

import (
	"gitlab.com/bawas07/echo-cms/config"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

// GetAlltag returl all record in table tags
func GetAllTag(model *[]models.Tag) error {

	db := config.DBConn()
	defer db.Close()
	err := db.Find(model).Error
	if err != nil {
		return err
	}
	return nil
}

// GetTagByID returl record in table tag based on id
func GetTagByID(model *models.Tag, id int) error {

	db := config.DBConn()
	defer db.Close()
	err := db.Joins("JOIN article_tags on article_tags.tag_id=tags.id").
		Joins("JOIN articles on articles.id=article_tags.article_id").
		Joins("JOIN categories on articles.category_id=categories.id").
		Group("tags.id").Preload("Articles").Preload("Articles.Category").
		First(model, id).Error
	if err != nil {
		return err
	}
	return nil
}

// StoreCategory used to store a record into table category
func StoreTag(model *models.Tag) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Create(model).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateTag(model *models.Tag) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Save(model).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteTag(model *models.Tag) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Delete(model).Error
	if err != nil {
		return err
	}
	return nil
}
