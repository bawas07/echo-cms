package services

import (
	"gitlab.com/bawas07/echo-cms/config"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

// GetAllArticle returl all record in table articles
func GetAllArticle(model *[]models.Article) error {

	db := config.DBConn()
	defer db.Close()
	err := db.Find(model).Error
	if err != nil {
		return err
	}
	return nil
}

// GetArticleByID returl record in table tag based on id
func GetArticleByID(model *models.Article, id int) error {

	db := config.DBConn()
	defer db.Close()
	err := db.
		Joins("JOIN article_tags on articles.id=article_tags.article_id").
		Joins("JOIN tags on article_tags.tag_id=tags.id").
		Joins("JOIN categories on articles.category_id=categories.id").
		Group("tags.id").Preload("Tags").Preload("Category").
		First(model, id).Error
	if err != nil {
		return err
	}
	return nil
}

// StoreArticle used to store a record into table category
func StoreArticle(model *models.Article, tagIDs []int) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Create(model).Error
	if err != nil {
		return err
	}

	if len(tagIDs) > 0 {
		var tags []models.Tag
		err = db.Where("id IN (?)", tagIDs).Find(&tags).Error
		if err != nil {
			return err
		}

		err = db.Model(&model).Association("Tags").Append(tags).Error
		if err != nil {
			return err
		}
	}

	return nil
}

func UpdateArticle(model *models.Article, tagIDs []int) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Save(model).Error
	if err != nil {
		return err
	}

	if len(tagIDs) > 0 {
		var tags []models.Tag
		err = db.Where("id IN (?)", tagIDs).Find(&tags).Error
		if err != nil {
			return err
		}

		err = db.Model(&model).Association("Tags").Replace(tags).Error
		if err != nil {
			return err
		}
	}
	return nil
}

func DeleteArticle(model *models.Article) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Delete(model).Error
	if err != nil {
		return err
	}
	return nil
}
