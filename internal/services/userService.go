package services

import (
	"gitlab.com/bawas07/echo-cms/config"
	"gitlab.com/bawas07/echo-cms/internal/models"
)

// GetAllUser returl all record in table user
func GetAllUser(model *[]models.User) error {

	db := config.DBConn()
	defer db.Close()
	err := db.Find(model).Error
	if err != nil {
		return err
	}
	return nil
}

// GetUserByID returl record in table user based on id
func GetUserByID(model *models.User, id int) error {

	db := config.DBConn()
	defer db.Close()
	err := db.First(model, id).Error
	if err != nil {
		return err
	}
	return nil
}

// StoreModel used to store a record into table user
func StoreUser(model *models.User) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Create(model).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateUser(model *models.User) error {
	db := config.DBConn()
	defer db.Close()
	model.HashPassword()
	err := db.Save(model).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteUser(model *models.User) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Delete(model).Error
	if err != nil {
		return err
	}
	return nil
}

func FindByEmailUser(model *models.User, email string) error {
	db := config.DBConn()
	defer db.Close()
	err := db.Where("email = ?", email).First(&model).Error
	if err != nil {
		return err
	}
	return nil
}
