package helpers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/bawas07/echo-cms/internal/renderings"
)

// TemplateResponseSuccess gives json template
func TemplateResponseSuccess(code int, msg string, data interface{}) renderings.Response {
	var message string = "Process Success"
	if msg != "" {
		message = msg
	}

	return renderings.Response{
		Code:    code,
		Success: true,
		Message: message,
		Data:    &data,
	}
}

// ResponseSuccess is a function to gives response success
func ResponseSuccess(c echo.Context, code int, message string, data interface{}) error {
	res := TemplateResponseSuccess(code, message, data)
	return c.JSON(code, res)
}

// ResponseError is a function to gives response error
func ResponseError(c echo.Context, code int, message string, data interface{}) error {
	if message == "" {
		message = "Uh-Oh! Error found"
	}
	res := TemplateResponseSuccess(code, message, data)
	res.Success = false
	return c.JSON(code, res)
}
