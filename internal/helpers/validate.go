package helpers

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"gopkg.in/go-playground/validator.v9"
)

func Validate (data interface{}) error {
	var validate *validator.Validate
	validate = validator.New()

	err := validate.Struct(data)
	if err != nil {
		return err
	}

	return nil
}

func makeValidationMsg(str, field, param string) string {
	if param != "" {
		return fmt.Sprintf(str, field, param)
	}

	return fmt.Sprintf(str, field)
}

func ValidationErrorHandler (err error, report *echo.HTTPError) {
	if castedObject, ok := err.(validator.ValidationErrors); ok {
		for _, err := range castedObject {
			var str string
			switch err.Tag() {
			case "required":
				str = "%s is required"
			case "email":
				str = "%s is not valid email"
			case "gte":
				str = "%s value must be greater than %s"
			case "lte":
				str = "%s value must be lower than %s"
			case "oneof":
				str = "%s value must one of %s"
			case "min":
				str = "%s value must minimal length %s"
			case "max":
				str = "%s value must maximum length %s"
			}
			report.Message = makeValidationMsg(str, err.Field(), err.Param())
			break
		}
	}
}