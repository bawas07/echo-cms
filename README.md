# Golang Startet

This is a template for golang project 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

you need these software installed on your machine
- golang
- dep package manager
- IDE or editor (Goland or VSCode with GO extention)

### Initial Set-up

- Copy or clone this repository to $GOPATH/git.gits.id/RnD/WEB/golang-echo-starter
- run `go run cmd/main/server.go` to run the program locally

### Build
- to build the program run `go build cmd/main/server.go` to generate file `main` in current location
- to run the program `./main`

## Running the tests

In this project we use Testify to create the test. 

- To test the code, create file *_test.go and create a function with name that begin with "Test", for example "TestGetOneItem"
- To run the test type `go test -v`

## Folder Structure
```
.
├── cmd
│   └── main
│       └── server.go
├── db
└── internal
    ├── configs
    ├── controllers
    ├── helpers
    ├── middleware
    ├── models
    ├── renderings
    ├── routes
    ├── routes
    └── services
```

| File        | Description | 
| :---        |    :----   | 
| `cmd/main/server.go` | Main applications for this project    |
| `internal/config/*`   | Configuration file templates or default configs |
| `internal/controllers/*` | Contains controllers of the application. This is where all the logic lies    |
| `db/*` | Database functions like migration and such |
| `internal/helpers/*`    | Contains something that have a possibility to be use over and over. For example in here we create response.go that we call when we give response
| `internal/middlewares/*` | Contains function that are used as a middlewares like check token |
| `internal/models/*` | Contains models of the application. This is where database structure is written    |
| `internal/renderings/*` | Template for response |
| `internal/routes/*` | Routes definition |

## How to use
- When creating another endpoint group, make sure you add the group at api.go to register it
- To use automigration, make sure you register your modet at `db/migrate.go`
- To disable automigration, disable this code: `db.DBMigrate()` at `cmd/main.go`

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Echo](https://echo.labstack.com/guide) - The web framework used
* [GORM](http://gorm.io/docs/index.html) - Orm used
* [Testify] (https://godoc.org/github.com/stretchr/testify) - Testing library

## Contributing

## Versioning
* v1.0.1 = edit readme
* v1.0.0 = initial vertion

## Authors

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
