package config

import (
	"errors"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Db struct {
	host       string
	port       string
	user       string
	pass       string
	dbname     string
	dblocation string // only for sqlite
	conn       *gorm.DB
}

func (db Db) open(dialect string) *gorm.DB {
	if db.conn != nil && db.conn.DB() != nil && db.conn.DB().Ping() == nil {
		return db.conn
	}
	var connection string
	if dialect == "mysql" {
		connection = db.user + ":" + db.pass + "@tcp(" + db.host + ":" + db.port + ")/" + db.dbname + "?charset=utf8&parseTime=True&loc=Local"
	}
	if dialect == "postgres" {
		connection = "postgres://" + db.user + ":" + db.pass + "@" + db.host + ":" + db.port + "/" + db.dbname + "?sslmode=disable"
	}
	if dialect == "sqlite" {
		dialect = "sqlite3"
		connection = db.dblocation
	}
	if dialect == "mssql" {
		connection = "sqlserver://" + db.user + ":" + db.pass + "@" + db.host + ":" + db.port + "?database=" + db.dbname
	}
	if dialect == "" {
		panic(errors.New("Dialect not found"))
	}
	conn, err := gorm.Open(dialect, connection)
	if err != nil {
		panic(err)
	}
	db.conn = conn
	return db.conn

	// return nil
}

// DBConn is a function that create connection to the database
func DBConn() *gorm.DB {
	var DBConn Db = Db{
		host:       os.Getenv("DB_HOST"),
		port:       os.Getenv("DB_PORT"),
		user:       os.Getenv("DB_USER"),
		pass:       os.Getenv("DB_PASS"),
		dbname:     os.Getenv("DB_NAME"),
		dblocation: os.Getenv("DB_LOCATION"),
		conn:       nil,
	}
	return DBConn.open(os.Getenv("DB_DIALECT"))
}
