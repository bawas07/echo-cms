package db

import (
	"fmt"
	"os"

	"gitlab.com/bawas07/echo-cms/config"
	"gitlab.com/bawas07/echo-cms/internal/models"
	// "git.gits.id/face-recog-apps/fr-go-api/config"
	// "git.gits.id/face-recog-apps/fr-go-api/modules/user"
)

// AutoMigrate is function that automatically migrating table on database.
// Be aware that this migration won't delete or renaming existing table or field
func AutoMigrate() {
	fmt.Println("==========================================================================")
	fmt.Println("Using database dialect:", os.Getenv("DB_DIALECT"))
	fmt.Println("Connecting to", os.Getenv("DB_HOST"), ":", os.Getenv("DB_PORT"))
	fmt.Println("==========================================================================")
	fmt.Println("Migrating Databases ...")
	db := config.DBConn()
	defer db.Close()
	fmt.Println("Migrating User ...")
	db.AutoMigrate(&models.User{})
	fmt.Println("Migrating Category ...")
	db.AutoMigrate(&models.Category{})
	fmt.Println("Migrating Article ...")
	db.AutoMigrate(&models.Article{})
	fmt.Println("Migrating Tag ...")
	db.AutoMigrate(&models.Tag{})
	fmt.Println("Migration Databases Done")
}
