package main

import (
	"net/http"

	"github.com/joho/godotenv"

	log "github.com/sirupsen/logrus"
	"gitlab.com/bawas07/echo-cms/db"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/bawas07/echo-cms/internal/routes"
	"gopkg.in/go-playground/validator.v9"
)

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.validator.Struct(i)
}

func main() {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from machine environment")
	}

	// Run Auto Migration
	db.AutoMigrate()

	// initiate Echo
	e := echo.New()

	// use validator
	e.Validator = &CustomValidator{validator: validator.New()}

	// Allow CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{
			echo.GET,
			echo.HEAD,
			echo.PUT,
			echo.PATCH,
			echo.POST,
			echo.DELETE,
		},
	}))

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		//Format: "method=${method}, uri=${uri}, status=${status}\n",
		// Format: "time:${time_rfc3339_nano}, remote_ip:${remote_ip}, user_agent:${user_agent}, host:${host}, method:${method}, uri:${uri}, status:${status}, error:${error}\n",
		Format: "${method} ${uri} | time:${time_rfc3339_nano}, remote_ip:${remote_ip}, host:${host}, status:${status}, error:${error}\n",
	}))

	e.Use(middleware.Recover())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	api := e.Group("/api")
	apiCms := e.Group("/api-cms")
	routes.RouteAPI(api)
	routes.RouteCMSAPI(apiCms)
	e.Logger.Fatal(e.Start(":1323"))
}
